package ru.gb.tests.login;

import io.qameta.allure.Description;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.gb.base.BaseTest;
import ru.gb.listeners.AllureListener;

@Listeners(AllureListener.class)
public class CheckErrorTextTest extends BaseTest {

    public static final String VALID_EMAIL_ADDRESS = "Please enter at least 8 characters";

    @Test
    @Description("Проверяем сообщение об ошибке при отсутствии пароля.")
    public void CheckEmptyEmail() {
        openApp("pixel 10")
                .clickLoginMenuButton()
                .inputEmail()
                .clickLoginButton()
                .checkLoginErrorText(VALID_EMAIL_ADDRESS);
    }
}


