package ru.gb.locators;

import ru.gb.locators.interfaces.FormsPageLocators;
import ru.gb.locators.interfaces.LoginPageLocators;
import ru.gb.locators.interfaces.MainPageLocators;
import ru.gb.locators.interfaces.android.AndroidFormsLocators;
import ru.gb.locators.interfaces.android.AndroidLoginLocators;
import ru.gb.locators.interfaces.android.AndroidMainLocators;
import ru.gb.locators.interfaces.iOs.IosFormsLocators;
import ru.gb.locators.interfaces.iOs.IosLoginLocators;
import ru.gb.locators.interfaces.iOs.IosMainLocators;

public class LocatorService {
    public static final MainPageLocators MAIN_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android") ? new AndroidMainLocators() : new IosMainLocators();

    public static final LoginPageLocators LOGIN_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android") ? new AndroidLoginLocators() : new IosLoginLocators();

    public static final FormsPageLocators FORMS_PAGE_LOCATORS = System.getProperty("platform")
            .equals("Android") ? new AndroidFormsLocators() : new IosFormsLocators();
}
