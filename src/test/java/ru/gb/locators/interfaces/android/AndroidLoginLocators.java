package ru.gb.locators.interfaces.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.locators.interfaces.LoginPageLocators;

public class AndroidLoginLocators implements LoginPageLocators {
    @Override
    public By loginField() {
        return MobileBy.AccessibilityId("input-email");
    }

    @Override
    public By passField() {
        return MobileBy.AccessibilityId("input-password");
    }

    @Override
    public By loginButton() {
        return MobileBy.AccessibilityId("button-LOGIN");
    }

    @Override
    public By loginText() {
        return MobileBy.AccessibilityId("android:id/message");
    }

    @Override
    public By loginErrorText() {
        return MobileBy.xpath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView");
    }
}
