package ru.gb.locators.interfaces.android;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import ru.gb.locators.interfaces.FormsPageLocators;

public class AndroidFormsLocators implements FormsPageLocators {
    @Override
    public By formsSwitch() {
        return MobileBy.AccessibilityId("switch");
    }

    @Override
    public By statusSwitchText() {
        return MobileBy.AccessibilityId("switch-text");
    }
}
