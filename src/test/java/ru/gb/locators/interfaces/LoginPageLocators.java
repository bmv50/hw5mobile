package ru.gb.locators.interfaces;

import org.openqa.selenium.By;

public interface LoginPageLocators {
    By loginField();
    By passField();
    By loginButton();
    By loginText();
    By loginErrorText();
}
