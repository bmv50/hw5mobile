package ru.gb.locators.interfaces;

import org.openqa.selenium.By;

public interface FormsPageLocators {
    By formsSwitch();
    By statusSwitchText();
}
