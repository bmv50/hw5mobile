package ru.gb.locators.interfaces;

import org.openqa.selenium.By;

public interface MainPageLocators {
    By loginButton();
    By formsButton();
    By homeScreen();
}
