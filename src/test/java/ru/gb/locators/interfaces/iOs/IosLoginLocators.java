package ru.gb.locators.interfaces.iOs;

import org.openqa.selenium.By;
import ru.gb.locators.interfaces.LoginPageLocators;

public class IosLoginLocators implements LoginPageLocators {
    @Override
    public By loginField() {
        return null;
    }

    @Override
    public By passField() {
        return null;
    }

    @Override
    public By loginButton() {
        return null;
    }

    @Override
    public By loginText() {
        return null;
    }

    @Override
    public By loginErrorText() {
        return null;
    }
}
